#!/usr/bin/env node
import * as program from "commander";
import {createTypescriptLib} from "../createTypescriptLib";
import { registerCommands } from "../commands/index";
import { packageVersion } from "../common/packageVersion";
import chalk from "chalk";

program
  .version(packageVersion);

registerCommands(program);

console.log(chalk.magenta("@botique/cli"), chalk.magentaBright(packageVersion));
program.parse(process.argv);