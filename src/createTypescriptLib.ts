'use strict';
import * as Generator from "yeoman-generator";
import * as chalk from "chalk";
import * as yosay from "yosay";
import {createEnv} from "yeoman-environment";
import {join} from "path";
import * as rename from "gulp-rename";

class TypescriptLibGenerator extends Generator {
  private props: Generator.Answers;

  constructor(args, opts) {
    super(args, opts);

    this.sourceRoot(join(__dirname, "../assets/templates"));
  }

  async prompting() {
    this.log(
      yosay(`Welcome to the rad ${(chalk as any).red('generator-temp')} generator!`)
    );

    const prompts = [
      {
        type: 'input',
        name: 'name',
        message: 'Project name?',
        default: this.appname
      },
      {
        type: 'input',
        name: 'version',
        message: 'Version?',
        default: "0.1.0"
      },
      {
        type: 'input',
        name: 'description',
        message: 'Description?'
      }
    ];

    this.props = await this.prompt(prompts);
  }

  writing() {
    this.registerTransformStream(rename(path => {
      path.extname = '';
    }));

    this.fs.copyTpl(
      this.templatePath('ts-lib/**/*'),
      this.destinationPath(),
      this.props
    );
  }

  install() {
    return this.installDependencies({
      npm: false,
      bower: false,
      yarn: true
    });
  }

  rootGeneratorName() {
    return "ts-lib";
  }
}

export function createTypescriptLib() {
  const env = createEnv();
  env.registerStub(TypescriptLibGenerator, "ts");
  env.run("ts");
}