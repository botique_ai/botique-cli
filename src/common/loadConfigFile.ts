import * as findUp from "find-up";
import { Config } from "../commands/Config";
import * as relativeRequire from "require-relative";
import { basename } from "path";

export const DEFAULT_ENTRY_POINT = "./src/index.ts";

const defaultConfig: Partial<Config> = {
    entryPoints: [DEFAULT_ENTRY_POINT],
    targets: ["es2017"],
    isRepo: true
};

export async function loadConfigFile(): Promise<Config> {
    const configFilePath = await findUp("botique.config.js");

    let userConfig = {
        ...defaultConfig
    };

    if (configFilePath)  {
        const loadedConfig = relativeRequire(configFilePath, basename(configFilePath));
        userConfig = {
            ...userConfig,
            ...loadedConfig
        };
    }

    return userConfig as Config;
}