import { readFileSync, writeFileSync } from "fs";
import { reduce } from "lodash";
import { commands } from "../commands";
import { format } from "prettier";
import { packageVersion } from "./packageVersion";

export function assertPackageJson(props: {
    isRepo: boolean;
}) {
    let packageJson;

    try {
        packageJson = JSON.parse(readFileSync("./package.json", "utf8"));
    }
    catch (err) {
        packageJson = {};
    }
    
    packageJson.scripts = {
        ...packageJson.scripts,
        ...reduce(commands, (scripts, command) => ({
            ...scripts,
            [command.name]: "botique " + command.name
        }), {})
    };

    packageJson.dependencies = {
        ...packageJson.dependencies,
        tslib: "^1.9.2",
    };

    packageJson.devDependencies = {
        ...packageJson.devDependencies,
        "@botique/cli": "^" + packageVersion,
        "jest": "^22.4.4",
        "ts-jest": "^22.4.6",
        "@types/jest": "^23.0.0",
        "typescript": "^2.9.1"
    }

    if (props.isRepo) {
        packageJson.devDependencies = {
            ...packageJson.devDependencies,
            "husky": "^0.14.3",
            "lint-staged": "^7.2.0"
        }

        packageJson.scripts = {
            ...packageJson.scripts,
            precommit: "lint-staged"
        };

        packageJson["lint-staged"] = {
            "*.{ts,tsx,js,json,css,md}": ["prettier --write", "git add"]
        }
    }

    packageJson.jest = {
        transform: {
          "^.+\\.tsx?$": "ts-jest"
        },
        testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$",
        moduleFileExtensions: [
          "ts",
          "tsx",
          "js",
          "jsx",
          "json",
          "node"
        ]
      }

    packageJson.main = "./lib/es2017/index.js";
    packageJson.types = "./lib/es2017/index.d.ts";
    packageJson.browser = "./lib/es5/index.js";
    
    writeFileSync("package.json", format(JSON.stringify(packageJson), { parser: "json" }))
}