import { JsxEmit, ModuleKind, ModuleResolutionKind, ScriptTarget, CompilerOptions } from "typescript";
import { BuildTarget } from "..";

export const baseTypescriptCompilerOptions = {
    module: ModuleKind.CommonJS,
    emitDecoratorMetadata: true,
    experimentalDecorators: true,
    moduleResolution: ModuleResolutionKind.NodeJs,
    lib: [],
    skipLibCheck: true,
    skipDefaultLibCheck: true,
    jsx: JsxEmit.React,
    rootDir: "./src",
    inlineSourceMap: true,
    inlineSources: true,
    declaration: true,
    importHelpers: true,
    noUnusedParameters: true,
    noUnusedLocals: true
};

export const typescriptCompilerOptionsByBuildTarget: Map<BuildTarget, CompilerOptions> = new Map([
    ["es5" as BuildTarget, {
        ...baseTypescriptCompilerOptions,
        target: ScriptTarget.ES5,
        outDir: "lib/es5"
    }],
    ["es2017" as BuildTarget, {
        ...baseTypescriptCompilerOptions,
        target: ScriptTarget.ES2017,
        outDir: "lib/es2017"
    }]
]);