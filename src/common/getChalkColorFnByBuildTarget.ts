import chalk, { Chalk } from "chalk";
import { BuildTarget } from "..";

const chalkColorFnByBuildTarget = new Map<BuildTarget, Chalk>([
    ["es5" as BuildTarget, chalk.green],
    ["es2017" as BuildTarget, chalk.blue]
]);

export function getChalkColorFnByBuildTarget​​(target: BuildTarget) {
    return chalkColorFnByBuildTarget.get(target) || chalk.white
}