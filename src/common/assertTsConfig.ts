import { baseTypescriptCompilerOptions } from "./typescriptCompilerOptions";
import { writeFileSync } from "fs";
import { format } from "prettier";

export function assertTsConfig(entryPoints: string[]) {
    const tsConfig = {
        compilerOptions: {
            ...baseTypescriptCompilerOptions,
            lib: ["es2017", "dom"],
            module: "commonjs",
            moduleResolution: "node",
            skipLibCheck: false,
            skipDefaultLibCheck: false,
            jsx: "react",
            target: "es2017",
            noEmit: true,
            pretty: true,
            declaration: false
        },
        include: ["src/**/*", "typings/**/*"]
    };

    writeFileSync("tsconfig.json", format(JSON.stringify(tsConfig), { parser: "json" }));
}