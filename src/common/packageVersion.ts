const { version } = require("../../package.json");

export const packageVersion = version;