import { FormatDiagnosticsHost, sys } from "typescript";

export const formatHost: FormatDiagnosticsHost = {
    getCanonicalFileName: path => path,
    getCurrentDirectory: sys.getCurrentDirectory,
    getNewLine: () => sys.newLine,
}