import { CreateCommandOption } from "../commands/Command";

export const TypeOption = CreateCommandOption({
    name: "type",
    description: "The type of the current package to be built",
});