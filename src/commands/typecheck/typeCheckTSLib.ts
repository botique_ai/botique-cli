import { createProgram, flattenDiagnosticMessageText, FormatDiagnosticsHost, sys, formatDiagnosticsWithColorAndContext, ScriptTarget } from "typescript";
import { typescriptCompilerOptionsByBuildTarget, baseTypescriptCompilerOptions } from "../../common/typescriptCompilerOptions";
import { forEach } from "lodash";
import chalk from "chalk";
import { formatHost } from "../../common/formatHost";

export function typeCheckTSLib(entryPoints: string[], targets) {
    forEach(targets, target => {
        console.log(`Typechecking typescript library for ${target}...`);
        const typecheckingCompilerOptions = {
            ...typescriptCompilerOptionsByBuildTarget.get(target),
            lib: ["lib.es2017.d.ts", "lib.dom.d.ts"],
            skipLibCheck: false,
            skipDefaultLibCheck: false,
            noStrictGenericChecks: true,
        };
        const program = createProgram(entryPoints, typecheckingCompilerOptions)
        const diags = program.getSemanticDiagnostics();

        if (diags.length > 0) {
            console.log(formatDiagnosticsWithColorAndContext(diags, formatHost));
            process.exit(1);
        }
        else {
            console.log(chalk.green("🎊 Yay! No type errors! Now make sure there are no bugs..."));
        }

        console.log(`Finished type checking for ${target}.`);
    });
}