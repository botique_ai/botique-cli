import { CreateCommand } from "../Command";
import { typeCheckTSLib } from "./typeCheckTSLib";
import { assertTsConfig } from "../../common/assertTsConfig";

export const TypeCheckCommand = CreateCommand({
    name: "typecheck",
    description: "Type check current package",
    options: [],
    run({ type, targets, entryPoints }) {
        if (type === "tslib" || type === "tsapp") {
            assertTsConfig(entryPoints);
            typeCheckTSLib(entryPoints, targets);
        }
        else {
            console.error("Cannot typecheck unknown type of package", type);
        }
    }
});