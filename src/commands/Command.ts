import { Config } from "./Config";

export type CommandOption = {
    name: string;
    description: string;
    optional?: boolean;
}

export type Command = {
    name: string;
    description: string;
    options: Array<CommandOption>;
    run(config: Config, params);
}

export function CreateCommand(props: Command) {
    return props;
}

export function CreateCommandOption(props: CommandOption) {
    return props;
}