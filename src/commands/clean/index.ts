import { CreateCommand } from "../Command";
import chalk from "chalk";
import { clean } from "./clean";

export const CleanCommand = CreateCommand({
    name: "clean",
    description: "Clean build artificats",
    options: [],
    run() {
        clean();
        console.log(chalk.green("🛁 Clean!"))
    }
})