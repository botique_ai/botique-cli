import { CreateCommand } from "../Command";
import { run } from "jest-cli";

export const TestCommand = CreateCommand({
    name: "test",
    description: "Run tests on the current package",
    options: [],
    run() {
        run();
    }
});