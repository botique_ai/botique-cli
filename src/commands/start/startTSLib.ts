import { runAndWatchScript } from "@botique/smartmon";

export function startTSLib(script) {
    runAndWatchScript(script, ["--inspect=0.0.0.0:5858", script]);
}