import { CreateCommand, CreateCommandOption } from "../Command";
import { startTSLib } from "./startTSLib";

export const StartCommand = CreateCommand({
    name: "start",
    description: "Start running a script while watching all depenedencies and restarting on changes.",
    options: [
        CreateCommandOption({
            name: "script",
            description: "The script to start",
            optional: true
        }),
        CreateCommandOption({
            name: "target",
            description: "Start the script corresponding to the given target",
            optional: true
        })
    ],
    run({ type, targets }, { script, target }) {
        if (type === "tslib" || type === "tsapp") {
            let scriptToRun = script;

            if (!script && target) {
                scriptToRun = `./lib/${target}/index.js`;
            }
            else if (!script) {
                scriptToRun = `./lib/${targets[0]}/index.js`
            }

            startTSLib(scriptToRun);
        }
        else {
            console.error("Cannot start unknown type of package", type);
        }
    }
})