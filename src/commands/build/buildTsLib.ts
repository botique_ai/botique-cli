import {createProgram, formatDiagnosticsWithColorAndContext} from "typescript";
import { forEach } from "lodash";
import { typescriptCompilerOptionsByBuildTarget } from "../../common/typescriptCompilerOptions";
import { BuildTarget } from "../..";
import { formatHost } from "../../common/formatHost";

export function buildTsLib(entryPoints: string[], targets: Array<BuildTarget​​>) {
    forEach(targets, target => {
        console.log("Building typescript library for target", target);
        const {diagnostics} = createProgram(entryPoints, {
            ...typescriptCompilerOptionsByBuildTarget.get(target),
            lib: ["lib.es2017.d.ts", "lib.dom.d.ts"]
        }).emit();

        if (diagnostics.length > 0) {
            console.log(formatDiagnosticsWithColorAndContext(diagnostics, formatHost));
        }

        console.log("Finished building libaray for target", target);
    });
}