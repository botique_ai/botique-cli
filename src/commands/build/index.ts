import { CreateCommand, CreateCommandOption } from "../Command";
import { buildTsLib } from "./buildTsLib";
import { TypeOption } from "../../common/TypeOption";
import { assertTsConfig } from "../../common/assertTsConfig";
import { clean } from "../clean/clean";

export const BuildCommand = CreateCommand({
    name: "build",
    description: "Build the current packages into build artificats",
    options: [],
    run({ type, targets, entryPoints }) {
        if (type === "tslib" || type === "tsapp") {
            assertTsConfig(entryPoints);
            clean();
            buildTsLib​​(entryPoints, targets);
        }
        else {
            console.error("Cannot build unknown type of package", type);
        }
    }
});