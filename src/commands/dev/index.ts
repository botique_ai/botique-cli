import { CreateCommand, CreateCommandOption } from "../Command";
import { TypeOption } from "../../common/TypeOption";
import { devTsLib } from "./devTsLib";
import { assertTsConfig } from "../../common/assertTsConfig";
import { devTsApp } from "./devTsApp";

export const DevCommand = CreateCommand({
    name: "dev",
    description: "Continously build current package for development",
    options: [],
    run({ type, targets, entryPoints }) {
        if (type === "tslib") {
            assertTsConfig(entryPoints);
            devTsLib(entryPoints, targets);
        }
        else if (type === "tsapp") {
            assertTsConfig(entryPoints);
            devTsApp();
        }
        else {
            console.error("Cannot dev unknown type of package", type);
        }
    }
})