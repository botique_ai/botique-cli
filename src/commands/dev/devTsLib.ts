import { createWatchCompilerHost, createWatchProgram, sys, createEmitAndSemanticDiagnosticsBuilderProgram, flattenDiagnosticMessageText, formatDiagnostic, Diagnostic, FormatDiagnosticsHost } from "typescript";
import chalk from "chalk";
import { forEach } from "lodash";
import { typescriptCompilerOptionsByBuildTarget } from "../../common/typescriptCompilerOptions";
import { getChalkColorFnByBuildTarget } from "../../common/getChalkColorFnByBuildTarget";
import { BuildTarget } from "../..";
import {format} from "date-fns";

export function devTsLib(entryPoints: string[], targets: Array<BuildTarget>) {
    forEach(targets, target => {
        const host  = createWatchCompilerHost(
            entryPoints, 
            typescriptCompilerOptionsByBuildTarget.get(target),
            sys, 
            createEmitAndSemanticDiagnosticsBuilderProgram,
            reportDiagnostic, 
            reportWatchStatusChanged.bind(undefined, getChalkColorFnByBuildTarget(target), target)
        );
        createWatchProgram(host);
    });
}

const formatHost: FormatDiagnosticsHost = {
    getCanonicalFileName: path => path,
    getCurrentDirectory: sys.getCurrentDirectory,
    getNewLine: () => sys.newLine,
}

function reportDiagnostic(diagnostic: Diagnostic) {
    // console.error("Error", diagnostic.code, ":",
    //     flattenDiagnosticMessageText(diagnostic.messageText, formatHost.getNewLine())
    // );
}

/**
 * Prints a diagnostic every time the watch status changes.
 * This is mainly for messages like "Starting compilation" or "Compilation completed".
 */
function reportWatchStatusChanged(colorFn, type: string, diagnostic: Diagnostic) {
    console.info(`[${chalk.yellow(format(new Date(), "YYYY-MM-DD HH:mm:ss.SSS"))} ${chalk.yellow(type)}]: ${colorFn(formatDiagnostic(diagnostic, formatHost))}`);
}