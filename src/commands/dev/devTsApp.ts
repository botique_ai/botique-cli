import { devTsLib } from "./devTsLib";
import { DEFAULT_ENTRY_POINT } from "../../common/loadConfigFile";
import { startTSLib } from "../start/startTSLib";

export function devTsApp() {
    devTsLib([DEFAULT_ENTRY_POINT], ["es2017"]);
    startTSLib("./lib/es2017/index.js");
}