import { BuildTarget } from "../common/BuildTarget";

export type ProjectType = "tslib" | "tsapp";

export type Config = {
    type: ProjectType;
    targets: Array<BuildTarget>;
    entryPoints?: string[];
    isRepo?: boolean;
};