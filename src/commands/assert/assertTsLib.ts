import { assertTsConfig } from "../../common/assertTsConfig";
import { assertPackageJson } from "../../common/assertPackageJson";
import chalk from "chalk";

export function assertTsLib(isRepo: boolean, entryPoints: string[]) {
  console.log("Asserting tsconfig.json...");
  assertTsConfig(entryPoints);

  console.log("Asserting package.json...");
  assertPackageJson({ isRepo });
  console.log(chalk.green("🎖 Fully asserted."));
}
