import { CreateCommand } from "../Command";
import { assertTsLib } from "./assertTsLib";

export const AssertCommand = CreateCommand({
    name: "assert",
    description: "Assert configurations in the current package",
    options: [],
    run({ type, entryPoints, isRepo }) {
        if (type === "tslib" || type === "tsapp") {
            assertTsLib(isRepo, entryPoints);
        }
        else {
            console.error("Can't assert package of unknown type: ", type);
        }
    }
})