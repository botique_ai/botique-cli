import { CommanderStatic } from "commander";
import { BuildCommand } from "./build";
import { Command, CommandOption } from "./Command";
import { DevCommand } from "./dev";
import { loadConfigFile } from "../common/loadConfigFile";
import { TypeCheckCommand } from "./typecheck";
import { StartCommand } from "./start";
import { AssertCommand } from "./assert";
import { CleanCommand } from "./clean";
import { TestCommand } from "./test";

const config = loadConfigFile();

export const commands: Array<Command> = [
    BuildCommand,
    DevCommand​​,
    TypeCheckCommand,
    StartCommand,
    AssertCommand,
    CleanCommand,
    TestCommand
];

export function registerCommands(program: CommanderStatic) {
    commands.forEach(command => registerCommand(command, program));
}

export function registerCommand(command: Command​​, program: CommanderStatic) {
    let createdCommand = program
        .command(command.name);

    createdCommand = command.options.reduce(
        (createdCommand, option) => createdCommand.option(`-${option.name[0]}, --${option.name} ${optionalize(option)}`, option.description),
         createdCommand
    );

    createdCommand.action(async (cmd) => {
        const params = command.options.reduce((params, option) => ({
            ...params,
            [option.name]: cmd[option.name]
        }), {});

        command.run(await config, params);
    })
}

function optionalize(option: CommandOption) {
    if (option.optional) {
        return `[${option.name}]`;
    }
    else {
        return `<${option.name}>`;
    }
}